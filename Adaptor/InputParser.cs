﻿using Adaptor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptor
{
	/// <summary>
	/// Abstract superclass to construct
	/// input parsers.
	/// </summary>
	public class InputParser
	{
		public virtual void RoomParser		(List<RoomObject> rooms, string folder)			{}
		public virtual void TeacherParser	(List<TeacherObject> teachers, string folder)	{}
		public virtual void CourseParser	(List<CourseObject> courses, string folder)		{}

		public void Parser(XmlResponse store, string folder)
		{
			var xmlToObject = new XmlToObject();
			var roomList	= xmlToObject.RoomList(store.XmlRooms);
			var teacherList = xmlToObject.TeacherList(store.XmlTeachers);
			var courseList	= xmlToObject.CourseList(store.XmlCourses);

			RoomParser		(roomList, folder);
			TeacherParser	(teacherList, folder);
			CourseParser	(courseList, folder);
		}
	}
}