﻿using Adaptor.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Adaptor
{
	/// <summary>
	/// Class that creates objects from the xml output from CentrisAPI.
	/// </summary>
	public class XmlToObject
	{
		/// <summary>
		/// Parses the xml room output from database and picks out the information that is needed.
		/// Returns a list of RoomObjects
		/// </summary>
		public List<RoomObject> RoomList(String s)
		{
			XmlReader reader = XmlReader.Create(new StringReader(s));
			List<RoomObject> roomList = new List<RoomObject>();
			String id ="", name="", seatCount="";

			while (reader.Read())
			{
				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "ID"))
				{
					id = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "Name"))
				{
					name = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "SeatCount"))
				{
					seatCount = reader.ReadInnerXml();
					roomList.Add(new RoomObject 
					{ 
						RoomID = id, 
						RoomName = name, 
						RoomSeatCount = seatCount 
					});
				}
			}

			return roomList;
		}

		/// <summary>
		/// Parses the xml teachers output from database and picks out the information that is needed.
		/// Returns a list of TeacherObject.
		/// </summary>
		public List<TeacherObject> TeacherList(String s)
		{
			XmlReader reader = XmlReader.Create(new StringReader(s));
			List<TeacherObject> teacherList = new List<TeacherObject>();
			String id = "", name = "";

			while (reader.Read())
			{
				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "SSN"))
				{
					id = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "Name"))
				{
					name = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "PersonDTO"))
				{
					teacherList.Add(new TeacherObject
					{
						TeacherName = name,
						TeacherSSN = id
					});
				}
			}

			return teacherList;
		}

		/// <summary>
		/// Parses the xml subjects output from database and picks out the information that is needed.
		/// Returns a list of CourseObject
		/// </summary>
		public List<CourseObject> CourseList(String s)
		{
			XmlReader reader = XmlReader.Create(new StringReader(s));
			List<CourseObject> courseList = new List<CourseObject>();
			String id = "", name = "", courseID = "", registeredStudents = "", dateStart = "", dateEnd = "", teacherName = "", teacherSSN = "";
			List<KeyValuePair<String, String>> teachers = new List<KeyValuePair<String, String>>();

			while (reader.Read())
			{
				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "ID"))
				{
					id = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "Name"))
				{
					name = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "CourseID"))
				{
					teachers.Clear();
					courseID = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "DateBegin"))
				{
					dateStart = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "DateEnd"))
				{
					dateEnd = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "RegisteredStudents"))
				{
					registeredStudents = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "FullName"))
				{
					teacherName = reader.ReadInnerXml();
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "SSN"))
				{
					teacherSSN = reader.ReadInnerXml();
					teachers.Add(new KeyValuePair<String, String>(teacherSSN, teacherName));
				}

				if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "Teachers"))
				{
					courseList.Add(new CourseObject
					{
						CourseID = id,
						CourseName = name,
						RegisteredStudentsInCourse = registeredStudents,
						DateStarts = dateStart,
						DateEnds = dateEnd,
						Teachers = teachers.ToList()
					});
				}
			}

			return courseList;
		}
	}
}