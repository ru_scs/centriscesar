﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptor.Models
{
	/// <summary>
	/// TeacherObject contains all needed information of a teacher.
	/// </summary>
	public class TeacherObject
	{
		private String teacherName;
		private String teacherSSN;

		public TeacherObject()
		{
			
		}

		public String TeacherName
		{
			get
			{
				return teacherName;
			}
			set
			{
				teacherName = value;
			}
		}

		public String TeacherSSN
		{
			get
			{
				return teacherSSN;
			}
			set
			{
				teacherSSN = value;
			}
		}
	}
}
