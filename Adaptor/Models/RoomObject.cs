﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptor.Models
{
	/// <summary>
	/// RoomObject contains all needed information of a room.
	/// </summary>
	public class RoomObject
	{
		private String roomID;
		private String roomName;
		private String roomSeatCount;

		public RoomObject()
		{
			
		}

		public String RoomID
		{
			get
			{
				return roomID;
			}
			set
			{
				roomID = value;
			}
		}

		public String RoomName
		{
			get
			{
				return roomName;
			}
			set
			{
				roomName = value;
			}
		}

		public String RoomSeatCount
		{
			get
			{
				return roomSeatCount;
			}
			set
			{
				roomSeatCount = value;
			}
		}
	}

}
