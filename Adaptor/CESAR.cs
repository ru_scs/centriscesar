﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adaptor
{
	public partial class CESAR : Form
	{
		public CESAR()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Gathers the input from the user and runs the process 
		/// needed to gather the data needed to import in 
		/// a timetabler software
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private async void Input_Click(object sender, EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;
			if (textBox1.Text == "")
			{
				System.Windows.Forms.MessageBox.Show("Please choose where to save the files");
				return;
			}

			if (comboBox1.Text == "")
			{
				System.Windows.Forms.MessageBox.Show("Please pick a software");
				return;
			}

			var url = ConfigurationManager.AppSettings["CentrisURL"];
			var token = ConfigurationManager.AppSettings["CentrisToken"];

			String semester = dateTimePicker1.Value.Year.ToString();
			if (dateTimePicker1.Value.Month > 5)
			{
				semester = semester + "3";
			}
			else if (dateTimePicker1.Value.Month > 2)
			{
				semester = semester + "2";
			}
			else
			{
				semester = semester + "1";
			}

			if (url == null)
			{
				Console.WriteLine("Failed to load API url");
			}
			
			String path = textBox1.Text;
			String sys = comboBox1.SelectedItem.ToString();
			String system = "Adaptor." + sys + "." + sys;
			try
			{
				await RequestData.HttpInitialize(url, token, semester, path, system);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				System.Windows.Forms.MessageBox.Show("Could not connect to Centris. Please make sure you are connected to Reykjavík University");

				return;
			} 
			System.Windows.Forms.MessageBox.Show("Done! Your files can be found at: " + path);
			Cursor.Current = Cursors.Default;
		}

		/// <summary>
		/// Gathers the input from the user and runs the processes 
		/// needed to book rooms in the system
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Output_Click(object sender, EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;
			if (textBox2.Text == "")
			{
				System.Windows.Forms.MessageBox.Show("Please choose the file that contains your timetable");
				return;
			}

			if (comboBox1.Text == "")
			{
				System.Windows.Forms.MessageBox.Show("Please pick a software");
				return;
			}

			var date = dateTimePicker1.Value;
			String semester = date.Year.ToString();
			if (date.Month > 5)
			{
				semester = semester + "3";
			}
			else if (date.Month > 2)
			{
				semester = semester + "2";
			}
			else
			{
				semester = semester + "1";
			}
			var sys = comboBox1.SelectedItem.ToString();
			String system = "Adaptor." + sys + "." + sys;
			var file = textBox2.Text;
			var year = date.Year.ToString();

			var outputParser = (OutputParser)Activator.CreateInstance(Type.GetType(system + "OutputParser"));
			outputParser.Parse(file, year);
			Cursor.Current = Cursors.Default;
		}

		/// <summary>
		/// Browse for folder button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button3_Click(object sender, EventArgs e)
		{
			string selectedPath = "";
			var t = new Thread((ThreadStart)(() =>
			{
				FolderBrowserDialog fbd = new FolderBrowserDialog();
				fbd.RootFolder = System.Environment.SpecialFolder.MyComputer;
				fbd.ShowNewFolderButton = true;
				if (fbd.ShowDialog() == DialogResult.Cancel)
					return;

				selectedPath = fbd.SelectedPath;
			}));

			t.SetApartmentState(ApartmentState.STA);
			t.Start();
			t.Join();
			Console.WriteLine(selectedPath);
			textBox1.Text = selectedPath;
		}

		/// <summary>
		/// Browse for file button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button4_Click(object sender, EventArgs e)
		{
			string selectedPath = "";
			var t = new Thread((ThreadStart)(() =>
			{
				OpenFileDialog fbd = new OpenFileDialog();
				if (fbd.ShowDialog() == DialogResult.Cancel)
					return;

				selectedPath = fbd.FileName;
			}));

			t.SetApartmentState(ApartmentState.STA);
			t.Start();
			t.Join();
			Console.WriteLine(selectedPath);
			textBox2.Text = selectedPath;
		}

		/// <summary>
		/// Tooltip messages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MouseHoverTooltip(object sender, System.EventArgs e)
		{
			ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
			ToolTip1.ShowAlways = true;
			ToolTip1.SetToolTip(this.button1, "Click to get information about classrooms, teachers and courses");
			ToolTip1.SetToolTip(this.button2, "Click to book rooms according to your timetable");
			ToolTip1.SetToolTip(this.button3, "Click to select a folder to save your files");
			ToolTip1.SetToolTip(this.button4, "Click to select your timetable");
		}
	}
}
