﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using API.Models;
using API.Models.Rooms;
using System.Globalization;
using System.Runtime.Serialization.Json;
using System.Configuration;

namespace Adaptor.Asc
{
	/// <summary>
	/// This is the AscOutputParser, it takes care of extracting
	/// necessary data from aScTimeTables to book a room for a 
	/// given timetable.
	/// </summary>
	public class AscOutputParser : OutputParser
	{
		private readonly IDbRequestInHandler _roomBooking;

		public AscOutputParser()
		{
			_roomBooking = new BookRooms();
		}

		public AscOutputParser(IDbRequestInHandler roomBooking)
		{
			_roomBooking = roomBooking;

		}

		/// <summary>
		/// The parser gets the needed attributes from the
		/// xml file that is delivered from aScTimeTables
		/// and uses them to create a booking object
		/// and calls the DbRequestInHandler to
		/// book a given room in CentrisAPI.
		public override void Parse(string file, String year)
		{
			if (Path.GetExtension(file) != ".xml")
			{
				System.Windows.Forms.MessageBox.Show("File type for asc must be an xml file");
				return;
			}

			var reader = XmlReader.Create(file);

			List<KeyValuePair<int, BookingViewModel>> courseOfBooking = new List<KeyValuePair<int, BookingViewModel>>();

			Hashtable roomId = new Hashtable();
			Hashtable periodStartId = new Hashtable();
			Hashtable periodEndId = new Hashtable();
			Hashtable courseLength = new Hashtable();
			Hashtable courseStarts = new Hashtable();
			Hashtable courseEnds = new Hashtable();
			Hashtable courseName = new Hashtable();

			String startTimeString = "", endTimeString = "", roomName = "";

			DateTime startTime, endTime;

			while (reader.Read())
			{
				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "subject"))
				{
					String courseID = reader.GetAttribute("id");
					startTimeString = reader.GetAttribute("customfield1");
					endTimeString = reader.GetAttribute("customfield2");
					courseName.Add(courseID, reader.GetAttribute("name"));

					// End time should not be null but if it happens, end time will be set as start time.
					if (endTimeString == null)
					{
						endTimeString = startTimeString;
					}

					startTime = DateTime.Parse(startTimeString);

					if (endTimeString == "")
					{
						endTimeString = startTimeString;
					}

					// Figuring out how many weeks the course spans
					endTime = DateTime.Parse(endTimeString);
					System.TimeSpan difference = endTime.Subtract(startTime);

					courseLength.Add(courseID, ((difference.Days / 7) + 1));
					courseStarts.Add(courseID, startTimeString);
					courseEnds.Add(courseID, endTimeString);
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "classroom"))
				{
					roomId.Add(reader.GetAttribute("id"), reader.GetAttribute("name"));
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "period"))
				{
					periodStartId.Add(reader.GetAttribute("period"), reader.GetAttribute("starttime"));
					periodEndId.Add(reader.GetAttribute("period"), reader.GetAttribute("endtime"));
				}

				if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "card"))
				{
					String subjectId = reader.GetAttribute("subjectid");

					int courseLn = Convert.ToInt16(courseLength[subjectId]);
					String classroomIds = "0";
					
					if (reader.GetAttribute("classroomids").Length != 0)
					{
						classroomIds = reader.GetAttribute("classroomids");
					}

					String startPeriod = periodStartId[(reader.GetAttribute("period"))].ToString();
					String endPeriod = periodEndId[(reader.GetAttribute("period"))].ToString();

					if (roomId[(reader.GetAttribute("classroomids"))] == null)
					{
						roomName = roomId["0"].ToString();
					}
					else
					{
						roomName = roomId[(reader.GetAttribute("classroomids"))].ToString();
					}

					if (startPeriod.Length == 4)
					{
						startPeriod = "0" + startPeriod;
					}

					if (endPeriod.Length == 4)
					{
						endPeriod = "0" + endPeriod;
					}
					String[] classroomId = classroomIds.Split(',');

					// Constructing the start date-time and end date-time for booking.
					String[] result = Regex.Split(courseStarts[subjectId].ToString(), "(T)");
					startTimeString = result[0] + "T" + startPeriod + ":00.000Z";
					endTimeString = result[0] + "T" + endPeriod + ":00.000Z";
					String format = "yyyy-MM-ddTHH:mm:ss.fffZ";
					CultureInfo provider = CultureInfo.InvariantCulture;
					startTime = DateTime.ParseExact(startTimeString, format, provider);
					endTime = DateTime.ParseExact(endTimeString, format, provider);

					// Adds days to get the correct date date when the course begins in the given week
					String weekDay = (reader.GetAttribute("days"));
					switch (weekDay)
					{
						case "1000000":
							break;
						case "0100000":
							startTime = startTime.AddDays(1);
							break;
						case "0010000":
							startTime = startTime.AddDays(2);
							break;
						case "0001000":
							startTime = startTime.AddDays(3);
							break;
						case "0000100":
							startTime = startTime.AddDays(4);
							break;
						case "0000010":
							startTime = startTime.AddDays(5);
							break;
						case "0000001":
							startTime = startTime.AddDays(6);
							break;
					}

					if (courseLn < 2)
					{
						BookingViewModel booking = new BookingViewModel
						{
							CourseID = Convert.ToInt16(subjectId),
							BookingType = 1,
							Bookings = new List<BookingDTO>
							{
								new BookingDTO 
								{
									StartTime = startTime,
									EndTime   = endTime,
									RoomName  = roomName
								}
							}
						};
						foreach(String item in classroomId)
						{
							courseOfBooking.Add(new KeyValuePair<int, BookingViewModel>(Convert.ToInt32(item), booking));
						}
					}
					else
					{
						BookingViewModel booking = new BookingViewModel
						{
							CourseID = Convert.ToInt16(subjectId),
							BookingType = 1,
							Recurrence = new RecurrenceViewModel
							{
								Type = 2,
								Occurs = courseLn,
								Begins = startTime,
								Ends = endTime
							}
						};

						switch (weekDay)
						{
							case "1000000":
								booking.Recurrence.WeekMonday = true;
								break;
							case "0100000":
								booking.Recurrence.WeekTuesday = true;
								break;
							case "0010000":
								booking.Recurrence.WeekWednesday = true;
								break;
							case "0001000":
								booking.Recurrence.WeekThursday = true;
								break;
							case "0000100":
								booking.Recurrence.WeekFriday = true;
								break;
							case "0000010":
								booking.Recurrence.WeekSaturday = true;
								break;
							case "0000001":
								booking.Recurrence.WeekSunday = true;
								break;
						}
						foreach (String item in classroomId)
						{
							courseOfBooking.Add(new KeyValuePair<int, BookingViewModel>(Convert.ToInt32(item), booking));
						}
					}
				}
			}
			_roomBooking.PostBooking(courseOfBooking, courseName);
		}
	}
}