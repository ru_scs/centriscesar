﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptor
{
	public interface DataStore
	{
		String     XmlRooms       { get; set; }
		String     XmlCourses     { get; set; }
		String     XmlTeachers    { get; set; }
	}

	/// <summary>
	/// XmlResponse stores the data received from Centris API
	/// </summary>
	public class XmlResponse : DataStore
	{
		private String     _XmlRooms;
		private String     _XmlSubject;
		private String     _XmlTeacher;
		
		public XmlResponse()
		{
			_XmlRooms   = "";
			_XmlSubject = "";
			_XmlTeacher = "";
		}

		public XmlResponse(String xmlRooms, String xmlSubject, String xmlTeacher)
		{
			_XmlRooms   = xmlRooms;
			_XmlSubject = xmlSubject;
			_XmlTeacher = xmlTeacher;
		}

		public String XmlRooms
		{
			get
			{
				return _XmlRooms;
			}
			set
			{
				_XmlRooms = value;
			}
		}

		public String XmlTeachers
		{
			get
			{
				return _XmlTeacher;
			}
			set
			{
				_XmlTeacher = value;
			}
		}

		public String XmlCourses
		{
			get
			{
				return _XmlSubject;
			}
			set
			{
				_XmlSubject = value;
			}
		}
	}
}