﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adaptor;
using API.Models;
using API.Models.Rooms;
using System.Collections;

namespace AdaptorTest
{
	public class MockDbRequestInHandler : IDbRequestInHandler
	{
		// PostBooking(List<KeyValuePair<int, BookingViewModel>> courseOfBooking, Hashtable courseName)

		public List<KeyValuePair<int, BookingViewModel>> Bookings { get; set; }

		public MockDbRequestInHandler()
		{
			Bookings = new List<KeyValuePair<int, BookingViewModel>>();
		}

		void IDbRequestInHandler.PostBooking(List<KeyValuePair<int, BookingViewModel>> courseOfBooking, Hashtable courseName)
		{
			Bookings = courseOfBooking;
		}
	}
}