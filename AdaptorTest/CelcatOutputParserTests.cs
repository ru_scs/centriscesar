﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Adaptor;
using Adaptor.Celcat;
using System.IO;
using System.Text;

namespace AdaptorTest
{
	[TestClass]
	public class CelcatOutputParserTests
	{
		private OutputParser _parser;
		private MockDbRequestInHandler _mockDbHandler;

		[TestInitialize]
		public void Setup()
		{
			_mockDbHandler = new MockDbRequestInHandler();
			_parser = new CelcatOutputParser(_mockDbHandler);
		}

		[TestMethod]
		public void CelcatOutputParserTestNoRecurrence()
		{
			// Arrange:
			string testCsv = "CT_MODULE\n_module_id,_unique_name,_name,_academic_year,_deptName,_staff1Name,_staff2Name,_tag1,_tag2,_tag3,_weekly_target,_total_target,_schedulable,_www,_notes\n5,21985,Advanced Legal English,,,,,,,,,,Y,,\n\nCT_ROOM\n_room_id,_unique_name,_name,_deptName,_siteName,_area,_staff1Name,_staff2Name,_tag1,_tag2,_tag3,_schedulable,_auto_schedulable,_default_capacity,_charge,_tel,_www,_deaf_loop,_wheelchair_access,_notes\n89,188,V108,,,,,,,,,Y,Y,48,,,,N,N,\n\nCT_EVENT\n_event_id,_day_of_week,_start_time,_end_time,_weeks,_event_name,_break_mins,_spanName,_event_catName,_tag1,_tag2,_tag3,_capacity_req,_deptName,_global_event,_protected,_suspended,_grouping_id,_registers_req,_lock,_notes,_resType,_resName,_resWeeks,_layoutName,_quantity\n168,Mon,09:00,10:00,1-1,,0,,,,,,,,N,N,N,,,,\n,,,,,,,,,,,,,,,,,,,,,Module,21985,,,\n,,,,,,,,,,,,,,,,,,,,,Room,188,,,";
			String year = "2012";
			System.IO.StreamWriter write = new System.IO.StreamWriter("celcat no rec test.csv");
			write.WriteLine(testCsv);
			write.Flush();
			write.Close();

			// Act:
			_parser.Parse(Path.GetFullPath("celcat no rec test.csv"), year);

			// Assert:
			Assert.AreEqual(1, _mockDbHandler.Bookings.Count, "The amount of booking objects with no recurrence is not correct!");
		}

		[TestMethod]
		public void CelcatOutputParserTestRecurrence()
		{
			// Arrange:
			string testCsv = "CT_MODULE\n_module_id,_unique_name,_name,_academic_year,_deptName,_staff1Name,_staff2Name,_tag1,_tag2,_tag3,_weekly_target,_total_target,_schedulable,_www,_notes\n5,21985,Advanced Legal English,,,,,,,,,,Y,,\n\nCT_ROOM\n_room_id,_unique_name,_name,_deptName,_siteName,_area,_staff1Name,_staff2Name,_tag1,_tag2,_tag3,_schedulable,_auto_schedulable,_default_capacity,_charge,_tel,_www,_deaf_loop,_wheelchair_access,_notes\n89,188,V108,,,,,,,,,Y,Y,48,,,,N,N,\n\nCT_EVENT\n_event_id,_day_of_week,_start_time,_end_time,_weeks,_event_name,_break_mins,_spanName,_event_catName,_tag1,_tag2,_tag3,_capacity_req,_deptName,_global_event,_protected,_suspended,_grouping_id,_registers_req,_lock,_notes,_resType,_resName,_resWeeks,_layoutName,_quantity\n168,Mon,09:00,10:00,1-5,,0,,,,,,,,N,N,N,,,,\n,,,,,,,,,,,,,,,,,,,,,Module,21985,,,\n,,,,,,,,,,,,,,,,,,,,,Room,188,,,";
			String year = "2012";
			System.IO.StreamWriter write = new System.IO.StreamWriter("celcat rec test.csv");
			write.WriteLine(testCsv);
			write.Flush();
			write.Close();

			// Act:
			_parser.Parse(Path.GetFullPath("celcat rec test.csv"), year);

			// Assert:
			Assert.AreEqual(1, _mockDbHandler.Bookings.Count, "The amoung of booking objects with recurrence is not correct!");
		}
	}
}
